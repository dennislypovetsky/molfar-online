new Accordion('.webinars-list__list', {
  elementClass: 'webinars-item', // ac
  questionClass: 'webinars-item__title', // ac-q
  answerClass: 'webinars-item__body', // ac-a
  duration: 250,
  closeOthers: false
});
