module.exports = function () {
  $.gulp.task('responsive', function () {
    return $.gulp.src([
      'src/static/data-img/**/*.{png,jpg}',
      'src/blocks/**/*.{png,jpg}',
      '!src/blocks/**/*_*.{png,jpg}',
      '!src/blocks/favicon/**/*.{png,jpg}',
    ])
      .pipe($.gp.responsive({
        '**/*.png': [{
          width: 1920,
          rename: {
            suffix: '_1920'
          }
        }, {
          width: 1440,
          rename: {
            suffix: '_1440'
          }
        }, {
          width: 1080,
          rename: {
            suffix: '_1080'
          }
        }, {
          width: 720,
          rename: {
            suffix: '_720'
          }
        }, {
          width: 360,
          rename: {
            suffix: '_360'
          }
        }],
        '**/*.jpg': [{
          width: 1920,
          rename: {
            suffix: '_1920'
          }
        }, {
          width: 1440,
          rename: {
            suffix: '_1440'
          }
        }, {
          width: 1280,
          rename: {
            suffix: '_1280'
          }
        }, {
          width: 1080,
          rename: {
            suffix: '_1080'
          }
        }, {
          width: 720,
          rename: {
            suffix: '_720'
          }
        }, {
          width: 360,
          rename: {
            suffix: '_360'
          }
        }]
      }, {
          format: 'jpg',
          progressive: true,
          withMetadata: false,
          withoutEnlargement: true,
          errorOnEnlargement: false,
          errorOnUnusedConfig: false,
          errorOnUnusedImage: false
        })
      )
      .pipe($.gulp.dest('src/blocks'));
  });
}
