# О проекте
Сборка на `Gulp`. Разметка пишется на `pug`. Стили — `SCSS`. Для картинок используется ретинизация через `src-set` и ленивая загрузка. Используемые файлы см. в `package.json`

## Продакшн
Выходные файлы лежат в папке `build`. Папка каждый раз стирается и создаётся заново. Писать что-то внутри — бессмысленно. Если нужно что-то добавить — используем папку `src`.

## Разработка
Перед началом `npm i` для установки Галпа и всех зависимостей.

Команда `gulp` запускает сборку проекта. Команда `gulp build` — продакшн сборку со сжатием файлов и изображений.

### Структура
```bash
build/ #(сюда собирается `src`)
gulp/ #(здесь настраивается Галп)
node_modules/ #(сюда устанавливаются все сторонние модули, необходимые для разработки)
src/ #(здесь ведётся разработка)
  blocks/ #(отдельные блоки проекта, вроде header)
    menu/
      menu.js
      menu.pug
      menu.scss
  markup/ #(разметка и страницы проекта)
  scripts/ #(общие скрипты проекта)
  static/ #(общие файлы проекта, вроде og-image)
  styles/ #(файл стилей, `SCSS`-миксины и базовые стили, вроде типографики)
```

### pug
1. index-страница проекта ./src/markup/pages/online
2. Подключать новые миксины в ./src/markup/layouts/mixins
